import { View, Text, StyleSheet } from "react-native";

export default function Timer({time}){
  const formattedTime = `${Math.floor(time / 60)
    .toString()
    .padStart(2, "0")}:${(time % 60).toString().padStart(2, "0")}`;
  return (
    <View style={styles.container} >
      <Text style={styles.time}>{formattedTime}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:"#f2f2f2",
    borderRadius:10,
    height:"40%",
    alignItems:'center',
    justifyContent:'center',
    marginTop:15,
  },
  time:{
    fontSize:80,
    fontWeight:'bold',
  }
})