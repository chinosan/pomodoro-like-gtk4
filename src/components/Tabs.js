import { TouchableOpacity, View, Text, StyleSheet } from "react-native";
const options = ["Pomodoro","Sort Break", "Long Break"]

export default function Tabs({setCurrent, current, setTime}){
  function handlePress (i) {
    setCurrent(i)
    setTime(i==0 ? (25*60) : i==1 ? (15*60) : (5*60))
  }
  return (
    <View style={styles.container} >
      {
        options.map( (op,index) => (
          <TouchableOpacity
          key={index}
            style={[styles.tab,current==index && {borderWidth:3,borderColor:"white"}]}
            onPress={()=>handlePress(index)}
          >
            <Text style={{fontSize:16}} >{op}</Text>
          </TouchableOpacity>

        ))
      }
    </View>
  );
}
const styles = StyleSheet.create({
  container:{
    display:"flex",
    flexDirection:'row',
    marginTop:15,
  },
  tab:{
    width:"33%",
    padding:15,
    alignItems:"center",
    backgroundColor:"rgba(255,255,255,0.3)",
    borderRadius:10,
  }
})