import { StatusBar } from 'expo-status-bar';
import { Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Tabs from './src/components/Tabs';
import Timer from './src/components/Timer';
import {useState, useEffect} from 'react'

const COLORS = ["#f7dc69","#a2d9ce","#d7bde2"];

export default function App() {
  const [isActive, setIsActive] = useState(false)
  const [current, setCurrent] = useState(0)
  const [time, setTime] = useState(25 * 60)

  useEffect( ()=> {
    let interval;
    if(isActive){
      interval = setInterval(() => {
        setTime(time -1)
      }, 10);
      
    } else {
      clearInterval(interval)
    }

    if(time == 0){
      setIsActive(false)
      setTime(current== 0 ? 1500 : current == 1 ? 900 : 300 )
    }
    return  ()=>clearInterval(interval)
  },[isActive, time])
  
  return (
    <View 
    style={[styles.container,Platform.OS == "android" && {paddingTop:30},{backgroundColor:COLORS[current]}]}>
      <Text style={styles.title} >Pomodoro</Text>

      <Tabs setCurrent={setCurrent} current={current} setTime={setTime} />

      <Timer time={time} />

      <TouchableOpacity style={styles.button} onPress={()=>setIsActive(!isActive)} >
        <Text style={{fontSize:32,textAlign:'center'}} >{isActive ? "Stop" : "Start"}</Text>
      </TouchableOpacity>
      {/* <View styl ></View> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal:15
  },
  title:{
    fontSize:32,
    fontWeight:'bold',
    alignItems:'center'
  },
  button:{
    padding:15,
    borderRadius:10,
    backgroundColor:"black",
    marginTop:15,
  }
});
